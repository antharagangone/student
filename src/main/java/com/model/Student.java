package com.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Student {
	@Id@GeneratedValue
	private int stuId;			
	
	@Column(name="sname")
	private String stuName;	
	private String email;
	private String mobile;
	private String city;
	
	public Student() {
		super();
	}

	public Student(int stuId, String stuName, String email, String mobile, String city) {
		super();
		this.stuId = stuId;
		this.stuName = stuName;
		this.email = email;
		this.mobile = mobile;
		this.city = city;
	}

	public int getStuId() {
		return stuId;
	}
	public String getStuName() {
		return stuName;
	}
	public String getEmail() {
		return email;
	}
	public String getMobile() {
		return mobile;
	}
	public String getCity() {
		return city;
	}
	
	public void setStuId(int stuId) {
		this.stuId = stuId;
	}
	public void setStuName(String stuName) {
		this.stuName = stuName;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public void setCity(String city) {
		this.city = city;
	}
	

	@Override
	public String toString() {
		return "StudentDetails [stuId=" + stuId + ", stuName=" + stuName + ", email=" + email + ", mobile=" + mobile + ", city=" + city + "]";
	}	
}

